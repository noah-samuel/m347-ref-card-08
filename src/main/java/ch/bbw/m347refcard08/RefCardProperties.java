package ch.bbw.m347refcard08;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * RefCardProperties
 * Properties for the application.
 * Gets the properties from the application.properties file.
 *
 * @author Peter Rutschmann
 * @version 25.04.2023
 */
@Component
@ConfigurationProperties(prefix = "refcard")
@Data
public class RefCardProperties {
   private String theme;
   private String dbUrl;
   private String dbUser;
   private String dbPassword;
}
