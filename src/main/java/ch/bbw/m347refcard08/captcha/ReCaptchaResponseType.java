package ch.bbw.m347refcard08.captcha;

import lombok.Data;

/**
 * ReCaptchaResponseType
 * @author Peter Rutschmann
 * @version 24.04.2023
 */
@Data
public class ReCaptchaResponseType {
	private boolean success;
	private String challenge_ts;
	private String hostname;
}
