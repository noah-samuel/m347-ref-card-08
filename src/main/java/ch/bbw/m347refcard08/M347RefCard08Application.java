package ch.bbw.m347refcard08;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class M347RefCard08Application {
   public static void main(String[] args) {
      SpringApplication.run(M347RefCard08Application.class, args);
   }

}
