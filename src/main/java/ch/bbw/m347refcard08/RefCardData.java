package ch.bbw.m347refcard08;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * RefCardData
 * Properties for the application.
 * Gets the properties from the application.properties file.
 *
 * @author Peter Rutschmann
 * @version 25.04.2023
 */
@Component
@Data
public class RefCardData {
   private String captchaMessage="Captcha not validated yet.";
}
