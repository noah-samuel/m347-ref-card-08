# FROM nginx:alpine-slim

# COPY src/main/resources/templates/index.html /usr/share/nginx/html/index.html


FROM maven:latest as builder
RUN su

# COPY index.html .

WORKDIR /app

RUN apt-get update && apt-get install -y git

RUN git clone https://gitlab.com/noah-samuel/m347-ref-card-08.git

COPY . /app/m347-ref-card-08
RUN chmod 777 /app/m347-ref-card-08/*

WORKDIR /app/m347-ref-card-08

RUN mvn clean install

# ----------------------

# FROM nginx:alpine-slim
FROM openjdk:latest

# RUN apk --no-cache add openjdk11-jre

ENV JAVA_HOME=/usr/lib/jvm/java-11-openjdk

COPY --from=builder /app/m347-ref-card-08/target/m347-ref-card-08-0.0.1-SNAPSHOT.jar .

COPY --from=builder /app/m347-ref-card-08/src/main/resources/templates/index.html /usr/share/nginx/html

CMD ["java", "-jar", "m347-ref-card-08-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080


# FROM nginx:alpine-slim

# #RUN ls -la /app/m347-ref-card-08/*.jar

# # COPY --from=builder index.html /usr/share/nginx/html 

# RUN mkdir test

# COPY --from=builder /app/m347-ref-card-08/ /test/app/m347-ref-card-08/



# CMD ["java", "-jar", "/test/app/m347-ref-card-08/target/m347-ref-card-08-0.0.1-SNAPSHOT.jar"]


